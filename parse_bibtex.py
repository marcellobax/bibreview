# -*- coding: utf-8 -*-

# BibReview
# Copyright (C) 2012 Jean-Baptiste LAMY (jibalamy at free . fr)
# BibReview is developped by Jean-Baptiste LAMY, at LIM&BIO,
# UFR SMBH, Université Paris 13, Sorbonne Paris Cité.

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

__all__ = ["parse_bibtex"]


import sys, datetime, re
from bibreview.abbrev import *
from bibreview.model  import *

MONTH_NAMES = { "spr" : 3, "sum" : 6, "aut" : 9, "win" : 12, "jan" :  1, "feb" :  2, "mar" :  3, "apr" :  4, "may" :  5, "jun" :  6, "jul" :  7, "aug" :  8, "sep" :  9, "oct" : 10, "nov" : 11, "dec" : 12 }

SPACE_REGEXP  = re.compile(u"\\s+")
SPACE2_REGEXP = re.compile(u"[ \\t]+")
SPACE3_REGEXP = re.compile(u"\n[ \\t]+")

def parse_bibtex(base, s):
  s     = s.replace(u"{", u"\n{").replace(u"}", u"\n}")
  root  = []
  stack = [root]
  for line in s.split(u"\n"):
    if   line.startswith(u"{"):
      line = line[1:]
      l = []
      stack[-1].append(l)
      stack.append(l)
    elif line.startswith(u"}"):
      line = line[1:]
      del stack[-1]
    if line: stack[-1].append(line + "\n")

  i = 0
  while i < len(root):
    if root[i].startswith(u"@"):
      add_reference(base, root[i], root[i + 1])
      i += 1
    i += 1
  
  base.sort()
  return base

def add_reference(base, l0, l):
  reference = Reference()
  volume    = issue = pages = u""
  year      = month = day = 1
  i         = 0
  l0        = l0.replace(u"\xa0", u"")
  while i < len(l):
    l[i] = l[i].replace(u"\xa0", u"")
    attr = SPACE_REGEXP.sub(u"", l[i]).lower()
    if attr.endswith(u"="):
      i += 1
      if   attr == u"title=":    reference.title    = strip_spaces(flatten_list(l[i]))
      if   attr == u"booktitle=":reference.journal  = strip_spaces(flatten_list(l[i]))
      elif attr == u"journal=":  reference.journal  = strip_spaces(flatten_list(l[i]))
      elif attr == u"doi=":      reference.doi      = strip_spaces(flatten_list(l[i]))
      elif attr == u"volume=":   volume             = strip_spaces(flatten_list(l[i]))
      elif attr == u"pages=":    pages              = strip_spaces(flatten_list(l[i])).replace("--", "-")
      elif attr == u"number=":   issue              = strip_spaces(flatten_list(l[i]))
      elif attr == u"abstract=": reference.abstract = strip_spaces_multiline(flatten_list(l[i]))
      elif attr == u"year=":     year = int(flatten_list(l[i]))
      elif attr == u"month=":
        s = flatten_list(l[i]).split()
        try: month = MONTH_NAMES.get(s[0][:3].lower()) or int(s[0])
        except: month = 1
        if len(s) > 1: day = int(s[1])
      elif attr == u"author=":
        s = strip_spaces(flatten_list(l[i]))
        s = u"\n".join(i.strip() for i in s.split(u" and "))
        reference.set_authors(s)
    i += 1
    
  if reference.journal: reference.journal = u"%s%s" % (reference.journal[0].upper(), reference.journal[1:].lower())
  reference.vol_n_p = vol_n_p(volume, issue, pages)
  if year != 1: reference.pub_date = datetime.date(year, month, day)
  base.add_reference(reference)
  l0_l = l0.lower()
  if   l0_l.startswith("@article"):       reference.set_value_for_category("pub_type", "article", 0)
  elif l0_l.startswith("@inproceedings"): reference.set_value_for_category("pub_type", "conference", 0)
  elif l0_l.startswith("@book"):          reference.set_value_for_category("pub_type", "book", 0)
  elif l0_l.startswith("@inbook"):        reference.set_value_for_category("pub_type", "in_book", 0)
  elif l0_l.startswith("@phdthesis"):     reference.set_value_for_category("pub_type", "phd", 0)
  else:                                   reference.set_value_for_category("pub_type", "website", 0)
  return reference

def strip_spaces(s):
  return SPACE_REGEXP.sub(u" ", s).strip()

def strip_spaces_multiline(s):
  s = "".join(((line.endswith(u".") and "%s\n" % line) or line) for line in s.split(u"\n"))
  s = SPACE2_REGEXP.sub(u" ", s).strip()
  s = SPACE3_REGEXP.sub(u"\n", s)
  return s

def flatten_list(l):
  return u"".join(((isinstance(i, list) and flatten_list(i)) or i) for i in l)

COMMAND_LINE_FUNCS["--import-bibtex"] = lambda arg: parse_bibtex(Base(), open(arg).read().decode("utf8"))

