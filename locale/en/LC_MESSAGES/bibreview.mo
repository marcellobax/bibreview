��          �      �           	          #     6  	   :     D     K     W     _     h     m     �     �     �     �     �  
   �     �     �     �  	   �     �  �   �  *   �     �     �                     *     9     H     `     e     {     �     �     �     �  )   �     �     �       	                 	                                             
                                                  authors authors_frequency auto_export_bibtex doi epub_date hal_id insert_date journal keywords pmid postprocess_command pub_date request review_history review_mode root_tag search_tag sort_criteria text_export url user_name vol_n_p Project-Id-Version: BiBreview
Last-Translator: Jean-Baptiste LAMY <jibalamy@free.fr>
Language-Team: english <jibalamy@free.fr>
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
 authors
last name, first name
last name II authors frequency automatic Bibtex export DOI E-publication date HAL identifier(s)  insertion date journal / book keywords
separated by ; PMID postprocessor command publication date Request, URL or PMID(s) review history review mode tags Restrict view
to references with
this tag sort criteria plain text export URL User name vol(n°):pp 