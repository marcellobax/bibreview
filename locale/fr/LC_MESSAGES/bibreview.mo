��    s      �  �   L      �	      �	     �	     �	     �	     
     !
     ?
     [
  "   x
     �
  !   �
     �
     �
     �
     �
       !        9     O     T     a     f  
   r     }     �  
   �     �     �     �     �     �     �     �               -     3     7     ?  
   G     R     a     r     �     �     �     �     �     �     �     �     �     �     
  
     "        =     R     b     w     �     �     �     �     �     �          '     ,     L     a     f     n     w          �     �     �     �     �     �     �     �     �  	   �     �  
   �          
               "     +     7  
   <     G     L     Q     e     n     v     }     �     �     �  
   �     �     �     �     �     �     �  	   �     �  �   �  %   �     �     �  "   �  &   	  )   0  %   Z  %   �  .   �  &   �  .   �     +     3     K     P     X  A   n     �     �     �     �     �     �               /     ;     C  %   L     r      z     �     �     �     �     �     �  	   �  	   �     �            "   (     K     S     [     c     {     �  	   �     �     �     �     �     �     �  !        @     Y     p     �     �     �     �     �               >  2   J     }     �  	   �     �     �     �     �     �                    %     6     ;     ?     H     U     d     r     �     �     �     �     �     �  
   �     �     �     �          !     :     @  
   T     _  	   k  9   u  	   �     �     �     �     �     �     �     �     =   #   )       W   ?   r   A   b          P   q   M   1              h   V   !                 
      Z           d   :   g   o          &   ,       X   (      Y   R   >           D   +       B          6       /   2   c       3       5   8   4                 -   "   K   \       N       s                e       f   Q   O   S      ]                          0   j   7      I   T             .   _   ^      n   C       m       <   G       [   @   L   9       *          `       a                    H   p   U   $              E   %   '   J   	          i   l                  k   ;   F          %s / %s references retrieved...
 Accepted Add Add reference manually Add references from HAL... Add references from Pubmed... Add references in BibTeX... Analyse authors frequency... Analyse first authors frequency... Analyse journals frequency... Analyse last authors frequency... Article Authors by frequency: Base Base %s Bayes classifier Bayes leave-one-out test on title BibReview preferences Book Book chapter Cite Cite in LyX Classifier Classify current base Close without saving Conference Conflict Edit Edit in new window... Edit... Export selection as CSV... File Goto... Import BibTeX... Import PubMed XML... Merge New Open... Pending PhD thesis Preferences... Publication type Pubmed query: %s references
 Quit Redo Rejected Rejected by abstract Rejected by text Rejected by title Remove Restrict to Restrict to %s Review status Save Save as... Save modifications before closing? Save selection as... Sort by authors Sort by date (E-pub) Sort by date (insertion) Sort by date (publication) Sort by journal Sort by lexical proximity Sort by review date Sort by review status Sort by title Sort using neural network Tags Train Bayes classifier on title Train neural network Undo Website abstract address authors authors_frequency auto_export_bibtex by comment criteria cumulated_length date doi editor epub_date first author grouped by hal_id insert_date journal key keywords last author name occurences path pmid postprocess_command pub_date request review review_history review_mode root_tag search search_tag sort_criteria tags text text_export title url user_name vol_n_p Project-Id-Version: BiBreview
Last-Translator: Jean-Baptiste LAMY <jibalamy@free.fr>
Language-Team: français <jibalamy@free.fr>
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
 %s / %s réferences récupérées...
 Accepté Ajouter Ajouter une référence à la main Ajouter des références depuis HAL... Ajouter des références depuis Pubmed... Ajouter des références en BibTeX... Analyser la fréquence des auteurs... Analyser la fréquence des premiers auteurs... Analyser la fréquence des journaux... Analyser la fréquence des derniers auteurs... Article Auteurs par frequence : Base Base %s Classifieur Bayésien Tester le classifieur Bayésien en 'leave-one-out' sur les titres Préférences de BibReview Livre Chapitre de livre Citation Citer dans LyX Classifieur Classer la base Fermer sans enregistrer Conférence Conflit Édition Éditer dans une nouvelle fenêtre... Éditer Exporter la sélection en CSV... Fichier Aller à... Importer BibTeX... Importer PubMed XML... Fusion Nouveau Ouvrir... À revoir Thèse Préférences... Type de publication Recherche Pubmed : %s réferences
 Quitter Refaire Rejeté Rejeté sur le résumé Rejeté sur le texte Rejeté sur le titre Supprimer Restreindre à Restreint à %s Statut de revue Enregistrer Enregistrer sous... Enregistrer les modifications ? Enregistrer la sélection sous... Trier par premier auteur Trier par date d'E-pub Trier par date d'insertion Trier par date de publication Trier par journal Trier par proximité lexicale Trier par date de relecture Trier par statut de revue Trier par titre Trier avec le réseau neuronal Étiquettes Entraîner le classifieur Bayésien sur les titres Entraîner le réseau neuronal Annuler Site oueb Résumé Adresse Auteurs
Nom, prenoms
Nom PP Fréquence des auteurs Export BibTex automatique par Commentaire Critère Effectif cumulé Date DOI Éditeur Date d'E-pub premier auteur regroupé par identifiant(s) HAL Date d'insertion Journal / Livre Clef Mots-clefs
séparés par ; dernier auteur Nom occurences Fichier PMID Commande du post-processeur Date de publication Requête, URL ou PMID(s) Revue Historique de revue Mode revue Étiquettes Recherche Restreindre la vue
aux références avec
cette étiquette Trier par Étiquettes texte export textuel Titre URL Nom d'utilisateur Vol(n°):pp 